package main

import (
	"io"
	"sync"
	"sync/atomic"
	"time"
)

func TimeoutReader(r io.Reader) *timeoutReader {
	return &timeoutReader{
		r: r,
	}
}

type timeoutReader struct {
	sync.Mutex

	r       io.Reader
	timeout time.Duration
	timer   *time.Timer
	close   int32
}

func (r *timeoutReader) Reader() io.Reader {
	return r.r
}

func (r *timeoutReader) SetTimer(timer *time.Timer) *timeoutReader {
	r.Lock()
	defer r.Unlock()

	r.timer = timer

	return r
}

func (r *timeoutReader) SetTimeout(timeout time.Duration) *timeoutReader {
	r.Lock()
	defer r.Unlock()

	r.timeout = timeout

	if r.timer == nil {
		r.timer = time.NewTimer(r.timeout)
	}

	return r
}

func (r *timeoutReader) Read(p []byte) (int, error) {
	if atomic.LoadInt32(&r.close) != 0 {
		return 0, io.EOF
	}

	if r.timer != nil {
		r.timer.Reset(r.timeout)
	}

	return r.r.Read(p)
}

func (r *timeoutReader) Timeout() <-chan time.Time {
	if r.timer != nil {
		return r.timer.C
	}

	return nil
}

func (r *timeoutReader) Close() {
	atomic.StoreInt32(&r.close, 1)
}
