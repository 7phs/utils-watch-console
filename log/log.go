package log

import (
	"os"
	"fmt"
)

var (
	infoOut = os.Stdout
	errorOut = os.Stderr
)

func Info(a ... interface{}) {
	fmt.Fprintln(infoOut, a...)
}

func Warning(a ... interface{}) {
	fmt.Fprintln(infoOut, a...)
}

func Error(a ... interface{}) {
	fmt.Fprintln(errorOut, a...)
}
