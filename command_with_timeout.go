package main

import (
	"context"
	"io"
	"os"
	"os/exec"
	"time"

	"bitbucket.org/7phs/watch-console/log"
)

const (
	INFINITE_WAIT      = 1<<63 - 1
	TRY_TO_KILL        = 3
	TIMEOUT_AFTER_KILL = 500
	TRY_TO_AFTER_KILL  = 10
)

func CommandWithTimeout(ctx context.Context, command string, args ...string) *commandWithTimeout {
	return &commandWithTimeout{
		Cmd:   exec.Command(command, args...),
		ctx:   ctx,
		timer: time.NewTimer(time.Duration(INFINITE_WAIT)),
	}
}

type commandWithTimeout struct {
	Cmd *exec.Cmd

	ctx context.Context

	timer   *time.Timer
	timeout time.Duration

	timedOut *timeoutReader
	timedErr *timeoutReader
}

func (o *commandWithTimeout) init() (io.ReadCloser, io.ReadCloser, error) {
	// Create stdout, stderr streams of type io.Reader
	stdout, err := o.Cmd.StdoutPipe()
	if err != nil {
		log.Error("Error: %s", err)
		return nil, nil, err
	}

	stderr, err := o.Cmd.StderrPipe()
	if err != nil {
		log.Error("Error: %s", err)
		return nil, nil, err
	}

	return stdout, stderr, nil
}

func (o *commandWithTimeout) SetWaitTimeout(seconds int64) *commandWithTimeout {
	o.timeout = time.Duration(seconds) * time.Second

	if o.timedOut != nil {
		o.timedOut.SetTimeout(o.timeout)
	}

	if o.timedErr != nil {
		o.timedErr.SetTimeout(o.timeout)
	}

	o.timer.Reset(o.timeout)

	return o
}

func (o *commandWithTimeout) Timeout() <-chan time.Time {
	return o.timer.C
}

func (o *commandWithTimeout) Start() error {
	stdout, stderr, err := o.init()
	if err != nil {
		return err
	}

	if err := o.Cmd.Start(); err != nil {
		return err
	}

	o.timedOut = TimeoutReader(stdout).
		SetTimer(o.timer).
		SetTimeout(o.timeout)

	o.timedErr = TimeoutReader(stderr).
		SetTimer(o.timer).
		SetTimeout(o.timeout)

	go io.Copy(os.Stdout, o.timedOut)
	go io.Copy(os.Stderr, o.timedErr)

	go func() {
		if o.ctx != nil {
			select {
			case <-o.ctx.Done():
				log.Info("Finish")
				o.Kill()
			}
		}
	}()

	return nil
}

func (o *commandWithTimeout) Wait() chan bool {
	finished := make(chan bool)

	go func() {
		o.Cmd.Wait()
		log.Info("Command finished")
		finished <- true
	}()

	return finished
}

func (o *commandWithTimeout) Kill() error {
	o.timedOut.Close()
	o.timedErr.Close()

	pid := o.Cmd.Process.Pid

	for i := 0; i < TRY_TO_KILL; i++ {
		log.Info("Kill command")

		if err := o.Cmd.Process.Kill(); err != nil {
			log.Error("Failed to kill process", err)
			log.Info("Try to kill ", i+1, "/", TRY_TO_KILL)
		} else {
			return nil
		}
	}

	// wait to completely finish process
	for i := 0; i < TRY_TO_AFTER_KILL; i++ {
		if p, err := os.FindProcess(pid); err != nil || p == nil {
			break
		} else {
			log.Info("Check PID ", pid, ":", i+1, "/", TRY_TO_AFTER_KILL)
			time.Sleep(time.Duration(TIMEOUT_AFTER_KILL) * time.Millisecond)
		}
	}

	return os.ErrInvalid
}
