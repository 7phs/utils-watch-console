package main

import (
	"sync"
	"context"
)

type cancelContext struct {
	sync.Mutex

	cancel func()

	finish bool
}

func (o *cancelContext) MakeContext() (ctx context.Context) {
	o.Lock()
	defer o.Unlock()

	if o.finish {
		o.cancel = nil
		return
	}

	ctx, o.cancel = context.WithCancel(context.Background())

	return
}

func (o *cancelContext) Cancel() {
	o.Lock()
	defer o.Unlock()

	if o.cancel!=nil {
		o.cancel()
	}

	o.finish = true
}
