package main

import (
	"context"
	"flag"
	"os"
	"sync"
	"time"

	"bitbucket.org/7phs/watch-console/log"
	"bitbucket.org/7phs/tools/shutdown"
)

const (
	TIMEOUT = 10
)

var (
	finished sync.WaitGroup

	infiniteTimeout int64
	waitTimeout     int64
)

func run(ctx context.Context, command string, args ...string) error {
	cmd := CommandWithTimeout(ctx, command, args...).SetWaitTimeout(waitTimeout)

	if err := cmd.Start(); err != nil {
		log.Error("Failed to start command: %s", err)
		return err
	}

	log.Info("Run", command, args)

	for {
		select {
		case <-cmd.Wait():
			log.Info("Command finish")
			return os.ErrClosed
		case <-cmd.Timeout():
			log.Warning("Timeout command")

			cmd.Kill()

			return nil
		}
	}
}

func main() {
	defer shutdown.Shutdown()

	flag.Int64Var(&infiniteTimeout, "infinite", 0, "Duration between restart command")
	flag.Int64Var(&waitTimeout, "wait", TIMEOUT, "Duration between restart command")

	flag.Parse()
	args := flag.Args()
	if len(args) == 0 {
		log.Info("Run with command and args as parameters.")
		flag.PrintDefaults()
		return
	}

	var (
		cmd    = args[0]
		timer  = time.NewTimer(time.Duration(infiniteTimeout) * time.Second)
		finish = make(chan bool)
		cancel cancelContext
	)

	args = args[1:]

	complete := func() {
		close(finish)
		timer.Stop()
		cancel.Cancel()
		finished.Done()
	}

	finished.Add(1)
	shutdown.Register(func() {
		complete()
	})

	for {
		ctx := cancel.MakeContext()
		if ctx == nil {
			break
		}

		finished.Add(1)
		if err := run(ctx, cmd, args...); err != nil {
			if err != os.ErrClosed {
				log.Error("Failed to run", err)
			}
		}
		finished.Done()

		if infiniteTimeout <= 0 {
			complete()
			break
		} else {
			timer.Reset(time.Duration(infiniteTimeout) * time.Second)

			select {
			case <-timer.C:
			case <-finish:
			}
		}
	}

	log.Info("Finish")

	finished.Wait()
}
